$VMNAME = "rename"
$rand9 = "xkedguiwd"
$rand20 = "edfgkbqefvynaswikvrd" 
$rand8 = "ekchweto" 

VBoxManage setextradata $VMNAME "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVendor" "American Megatrends Inc"
VBoxManage setextradata $VMNAME "VBoxInternal/Devices/pcbios/0/Config/DmiBIOSVersion" "2.1.0"
VBoxManage setextradata $VMNAME "VBoxInternal/Devices/pcbios/0/Config/DmiSystemVendor" "ASUSTek Computer"

VBoxManage setextradata $VMNAME "VBoxInternal/Devices/pcbios/0/Config/DmiSystemSerial" $rand9

VBoxManage setextradata $VMNAME "VBoxInternal/Devices/ahci/0/Config/Port0/SerialNumber" $rand20

VBoxManage setextradata $VMNAME "VBoxInternal/Devices/ahci/0/Config/Port0/FirmwareRevision" $rand8
VBoxManage setextradata $VMNAME "VBoxInternal/Devices/ahci/0/Config/Port0/ModelNumber" "SEAGATE ST3750525AS"