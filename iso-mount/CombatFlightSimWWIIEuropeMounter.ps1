﻿# By TheCakeIsNaOH 07-12-2018

# Needs Wincdemu installed, "Set-ExecutionPolicy RemoteSigned" set in powershell, and.ps1 files set to open with powershell.exe as default. 
# WinCDEmu download- http://wincdemu.sysprogs.org/

# Put a single pair of double quotes around each of the three paths, it will not work if you have two pairs of quotes

# The easy way input the path is generally to navigate to the file in explorer, then shift+right click on the exe/iso, 
# then click copy as path, and finally paste in the appropriate line of the script.

#Path to WinCDEmu executeable used for command line operation. Default installed path is C:\Program Files (x86)\WinCDEmu\batchmnt.exe
$MntExePath = "C:\Program Files (x86)\WinCDEmu\batchmnt.exe"

#Path to game, include arguments if needed after exe
$GamePath = "C:\Program Files (x86)\Microsoft Games\Combat Flight Simulator\COMBATFS.EXE"

#Path to WinCDEmu compatible image
$ISOpath = "%userprofile%\ISO\CombatFlightSimulatorEurope.ISO"

#mount ISO
& $MntExePath $ISOpath

#start game and wait till exited
& $GamePath | Out-Null

#umount ISO
& $MntExePath /unmount $ISOpath