# Script for Mounting disk images while a program is running.
----

It is intended for games that want an optical disk in the drive to run. Dumping the disk to an ISO, then mounting it with a virtual drive program is a workaround for some games. This automates that by mounting the ISO, runing the exe, waiting till the exe exits and then unmounting the ISO.

WinCDEmu will not get around some more advanced checks for the original optical disk such as Securom. But many older games will work with WinCDEmu and therefore this script.

---

Requirements-
1. WinCDEmu- http://wincdemu.sysprogs.org/
2. 'Set-ExecutionPolicy RemoteSigned' set in powershell
3. (Strongly Recommended) .ps1 files set to open with powershell.exe as default

----

Put a single pair of double quotes around each of the three paths in the script, 
it will not work if you have two pairs of quotes around each path.

The easiest way input the path is generally to navigate to the file in explorer, then shift+right click on the exe/iso, then click copy as path, and finally ctrl+v in the appropriate line of the script.

---

It is your responsibility to use in accordance with the copyright laws in your country. 
