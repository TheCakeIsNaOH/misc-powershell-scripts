Runs on powershell 5+, works on Linux

Requires `youtube-dl` installed and on path

Copy the example files to a subdirectory under where you have `archive.ps1` placed. Then edit as needed, replacing options in `archive.config` with your prefered options, and changing `info.txt` to the the desired URL.

