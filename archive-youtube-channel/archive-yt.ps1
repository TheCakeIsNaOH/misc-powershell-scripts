#!/usr/bin/env pwsh
#change?

#param( 
#    [switch]$q
#)



$homedir = Split-Path $MyInvocation.MyCommand.Definition
[System.Collections.ArrayList]$archivesDirList = @()
$dirlist = Get-Childitem -Directory -Recurse -Path $homedir
$ymd = (Get-Date -Format "yyyyMMdd")

Start-Process -FilePath sudo -NoNewWindow -Wait -ArgumentList "youtube-dl -U"

$dirlist | ForEach-Object {
	#Write-Output $_.fullname
	$config = Join-Path $_.FullName 'archive.config'
	$infotext = Join-Path $_.FullName 'info.txt'
	if ((Test-Path $config) -and (Test-Path $infotext)) {
		$url = ((Get-Content $infotext) -split '`n' | sls -pattern 'http').ToString()

		Add-Member -InputObject $_ -NotePropertyName infotext -NotePropertyValue $infotext
		Add-Member -InputObject $_ -NotePropertyName config -NotePropertyValue $config
		Add-Member -InputObject $_ -NotePropertyName url -NotePropertyValue $url
		#Write-Output $_.url
		$archivesDirList.add($_) | Out-Null
	} 
}

$archivesDirList | ForEach-Object {
	$args = '-c "youtube-dl --config-location ' + "'" + $_.config + "' " + $_.url
	#Write-Host $args
	#if ($q) {
	#	$args = '--quiet ' + $args
	#}
    $args = $args +  " | grep -vi 'has already been recorded in archive\|\[download\] Downloading video'" + '"'
	Start-Process -FilePath bash -WorkingDirectory $_.fullname -NoNewWindow -Wait -ArgumentList $args
    (Get-Content -Path $_.infotext) -replace "Updated .*" , "Updated $ymd" | Set-Content -Path $_.infotext
    #Write-Host "Working directory: " $_.fullname
    #Write-Host "Args: " $args
	
}

#$default-config = Join-Path (Split-Path $MyInvocation.MyCommand.Definition) 'default.config'
#youtube-dl -U
#youtube-dl --config-file $default-config
