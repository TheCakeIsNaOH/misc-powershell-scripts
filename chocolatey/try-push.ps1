Function try-push {
	Param (
		[string]$dir = ".",
		[int]$separation = 300
	)

	If (!(Test-Path $dir)) {
		Throw "Invalid directory path $dir"
	}

	If ($separation -le 299) {
		Throw "Please make a larger separation, above 10 minutes"
	}

	while ($process.exitcode -ne 0) {
		$process = Start-Process -FilePath cpush -WorkingDirectory $dir -Wait -NoNewWindow -PassThru
		
		if ($process.exitcode -eq 0) {
			Add-Type -AssemblyName  System.Windows.Forms
			$global:balloon = New-Object System.Windows.Forms.NotifyIcon
			$path = (Get-Process -id $pid).Path
			$balloon.Icon = [System.Drawing.Icon]::ExtractAssociatedIcon($path)
			$balloon.BalloonTipText = 'Your package has been pushed'
			$balloon.BalloonTipTitle = "It worked" 
			$balloon.Visible = $true 
			$balloon.ShowBalloonTip(30000)
			}
			
		Start-Sleep -s $separation
		}
}