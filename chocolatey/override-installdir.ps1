param(
	[parameter(Mandatory=$false, Position=1)][string]$installDir = 'C:\',
	[parameter(Mandatory=$false, Position=0)][string]$pkgConfigPath = (Join-Path "$(Split-Path -parent $MyInvocation.MyCommand.Definition)" 'install.config')
)

if (!(Test-Path $installDir)) {
	Throw 'did not find override directory'
}

if (!(Test-Path $pkgConfigPath)) {
	Throw 'did not find package config file'
}

[xml]$pkgConfig = Get-Content $pkgConfigPath

$pkgConfig.packages.package.id | ForEach-Object {
	$overrideDir = (Join-Path $installDir $_) 
	#Write-Host $overrideDir
	If (!(Test-Path $overrideDir)) {
		$null = New-Item -ItemType Directory -Path $overrideDir
	}
	
	Start-Process -FilePath "choco" -ArgumentList "install $_ --install-directory=$overrideDir" -NoNewWindow -Wait
}