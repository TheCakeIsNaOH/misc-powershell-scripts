﻿$numRuns = 10
[decimal]$time = 0

for ($loop=1; $loop -le $numRuns; $loop++) { $time = $time + [decimal](Measure-Command { choco -h }).TotalSeconds }
write-host 'total time for ' $numRuns ' runs ' $time ' seconds'
write-host 'avagage time per run is ' ($time / $numRuns) ' seconds'
