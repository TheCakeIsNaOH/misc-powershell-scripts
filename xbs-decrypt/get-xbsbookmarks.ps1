﻿# Not completed, lzutf8 is JS only, so this is stuck
param (
	[parameter(Mandatory=$true)][string]$password,
	[parameter(Mandatory=$true)][string]$syncID,
	[string]$syncURL = "https://api.xbrowsersync.org"
)

Add-type -Path "\bouncycastle.1.8.6.1\lib\BouncyCastle.Crypto.dll"

. (Join-Path (Split-Path -parent $MyInvocation.MyCommand.Definition) 'New-PBKDF2Key.ps1')
. (Join-Path (Split-Path -parent $MyInvocation.MyCommand.Definition) 'Invoke-Win32Api.ps1')

<# $password = ''
$syncID   = ''
$syncData = "" #>


#$ErrorActionPreference = 'Stop'

$encoding = [system.Text.Encoding]::UTF8
$securePassword  = ConvertTo-SecureString -String $password -Force -AsPlainText
$syncIdBytes = $encoding.GetBytes($syncID)
$getUrl = $syncURL.trim("/") + "/bookmarks/" + $syncID

Try {
	$syncData = Invoke-RestMethod -Method Get -Uri $getURL -UseBasicParsing
} Catch {
	Write-Error "Retrieving encrypted data from server failed, please check that your sync ID and sync URL are correct, and the server is reachable"
}

$pbkBytes = New-PBKDF2Key -Algorithm SHA256 -Password $securepassword -Salt $syncIdBytes -Iterations 250000 -Length 32 
$pbkString = [System.Convert]::ToBase64String($pbkBytes) 
$syncDataBytes = [System.Convert]::FromBase64String($syncData.bookmarks)
$nonce = $syncDataBytes[0..15]
$tag = $syncDataBytes[($syncDataBytes.Length - 16)..($syncDataBytes.Length)]


function SimpleDecrypt {
   Param(
       [string]$encryptedMessage,
       [byte[]]$key,
       [int]$nonSecretPayloadLength = 0
   )
   if ([string]::IsNullOrEmpty($encryptedMessage)) {
       throw "Secret message Required!"
   }
   $encbyteMessage = [byte[]][convert]::FromBase64String($encryptedMessage)
   $cipherStream = [System.IO.MemoryStream]::new($encbyteMessage)
   $cipherReader = [System.IO.BinaryReader]::new($cipherStream)
   $nonSecretPayload = $cipherReader.ReadBytes($nonSecretPayloadLength)
   $nonce = $cipherReader.ReadBytes(16)
   $cipher = [Org.BouncyCastle.Crypto.Modes.GcmBlockCipher]::new([Org.BouncyCastle.Crypto.Engines.AesEngine]::new())
   $parameters = [Org.BouncyCastle.Crypto.Parameters.AeadParameters]::new([Org.BouncyCastle.Crypto.Parameters.KeyParameter]::new($key), 128, $nonce) #payload als byte!!!
   $cipher.Init($false, $parameters)
   $cipherText = $cipherReader.ReadBytes($encbyteMessage.Length - $nonSecretPayloadLength - $nonce.Length)
   $plainText = [byte[]]::new($cipher.GetOutputSize($cipherText.Length))
   $len = $cipher.ProcessBytes($cipherText, 0, $cipherText.Length, $plainText, 0)
   $null = $cipher.DoFinal($plainText, $len)
   return [system.Text.encoding]::UTF8.GetString($plainText)
}


$data = SimpleDecrypt -encryptedMessage $syncData.bookmarks -key $pbkBytes
$data.gettype()


$data = [System.Convert]::FromBase64String($base64data)
$ms = New-Object System.IO.MemoryStream
$ms.Write($data, 0, $data.Length)
$ms.Seek(0,0) | Out-Null

$sr = New-Object System.IO.StreamReader(New-Object System.IO.Compression.DeflateStream($ms, [System.IO.Compression.CompressionMode]::Decompress))

while ($line = $sr.ReadLine()) {  
    #$line
}










