Setup Powershell remoting stuff.

Enter a remote session-
$ip_address = 127.0.0.1
$session = (New-PSSession -ComputerName $ip_address -Credential admin)
enter-pssession -session $session

Directly enter session
enter-pssession -ComputerName 127.0.0.1 -Credential admin

Run a command
Invoke-Command -Session $session -ScriptBlock { Write-Output "stuff here" } 