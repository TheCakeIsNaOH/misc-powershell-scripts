#Setup on new computer-
#enable service
Set-Service WinRM -StartMode Automatic
Start-Service winrm

#set all connections to private network
Get-NetConnectionProfile | % { Set-NetConnectionProfile -NetworkCategory Private -InterfaceAlias $_.InterfaceAlias }

#check winrm status
winrm quickconfig

